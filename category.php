<?php get_template_part('templates/header'); ?>

<div class="container">
    <div class="content">
        <h1>This is category.php</h1>

        <p>This is output of: <?php single_cat_title('', true); ?></p>

        <?php get_template_part('templates/loop'); ?>
    </div>
</div>

<?php get_template_part('templates/loop'); ?>

<?php get_template_part('templates/footer'); ?>
