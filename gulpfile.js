/**
 * Requires dependencies
 */
var gulp = require('gulp');
var less = require('gulp-less');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var watch = require('gulp-watch');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var minifyCSS = require('gulp-minify-css');
var gutil = require('gulp-util');
var order = require('gulp-order');
var notify = require('gulp-notify');
var livereload = require('gulp-livereload');

require("time-require");

/**
 * LESS compiling
 * Just click and look!
 */
gulp.task('less', function () {
    gulp.src('static/less/main.less')
        .pipe(less())
        .on('error', notify.onError({
            message: "<%= error.message %>",
            title: "LESS error",
            sound: "Glass"
        }))
        .pipe(gulp.dest('build/css'))
        .pipe(rename('main.min.css'))
        .pipe(minifyCSS())
        .pipe(gulp.dest('build/css'))
        .pipe(livereload());
});

/**
 * Concatenate & Minify JS
 * Place your javascript files in order
 */
gulp.task('scripts', function () {
    gulp.src([
        './static/vendor/jquery/dist/jquery.js',
        './static/vendor/respond/dest/respond.min.js',
        './static/js/plugins/modernizr-svg.min.js',
        './static/vendor/snapjs/snap.min.js',
        './static/js/_app.js'
    ])
        .pipe(concat('app.js'))
        .pipe(gulp.dest('build/js'))
        .pipe(rename('app.min.js'))
        .pipe(uglify())
        .on('error', notify.onError({
            message: "<%= error.message %>",
            title: "JS error"
        }))
        .pipe(gulp.dest('build/js'))
        .pipe(livereload());
});

/**
 * Optimize all images
 * Default level is 3
 */
gulp.task('images', function () {
    return gulp.src('static/img/*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest('static/img'));
});

/**
 * Whenever something happen, reload.
 */
gulp.task('php', function () {
    livereload.changed();
});

/**
 * Watch everything
 */
gulp.task('watch', function () {
    gulp.watch('**/*.php', ['php']);
    gulp.watch('static/js/_*.js', ['scripts']);
    gulp.watch('static/js/plugins/*.js', ['scripts']);
    gulp.watch('static/less/**/*.less', ['less']);
    gulp.watch('static/img/*', ['images']);
});

/**
 * Livreload server start and watch over my project.
 */
gulp.task('default', function () {
    livereload.listen();
    gutil.beep();
    gulp.start('watch');

});