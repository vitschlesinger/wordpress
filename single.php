<?php get_template_part('templates/header'); ?>

<div class="container">
    <div class="content">
        <h1>This is single.php</h1>

        <?php get_template_part('templates/loop', 'single'); ?>

        <?php comments_template() ?>
    </div>
</div>

<?php get_template_part('templates/footer'); ?>
