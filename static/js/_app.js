$(document).ready(function() {
    function addEvent(to, type, fn){
        if(document.addEventListener){
            to.addEventListener(type, fn, false);
        } else if(document.attachEvent){
            to.attachEvent('on'+type, fn);
        } else {
            to['on'+type] = fn;
        }
    };

    $('#menu-pull').click(function(evt){
        evt.preventDefault();
        $(this).toggleClass('open');
        $('#menu').toggleClass('closed');
    });

    /**
     * SNAP.js - responsivní side navigace
     */


    if($('body').hasClass('snapjs-right')){
        console.log('bingo!');
    }

    /*  create cross-browser event handler. */
    var addEvent = function addEvent(element, eventName, func) {
        if (element.addEventListener) {
            return element.addEventListener(eventName, func, false);
        } else if (element.attachEvent) {
            return element.attachEvent("on" + eventName, func);
        }
    };

    /* initiate snapper object, and define pane to slide left or right */
    var snapper = new Snap({
        element: document.getElementById('content'),
        disable: 'left',
        touchToDrag: false
    });

    /* when the element with id 'ol' is clicked, use the public snapper.open() method to slide the pane rightward, revealing the menu on the LEFT */

    addEvent(document.getElementById('open-right'), 'click', function() {
        snapper.open('right');
    });

    /* Prevent Safari opening links when viewing as a Mobile App */
    (function(a, b, c) {
        if (c in b && b[c]) {
            var d, e = a.location,
                f = /^(a|html)$/i;
            a.addEventListener("click", function(a) {
                d = a.target;
                while (!f.test(d.nodeName))
                    d = d.parentNode;
                "href" in d && (d.href.indexOf("http") || ~d.href.indexOf(e.host)) && (a.preventDefault(), e.href = d.href)
            }, !1)
        }
    })(document, window.navigator, "standalone");

});