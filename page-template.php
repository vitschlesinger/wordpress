<?php
/*
Template Name: My Custom Page
*/
?>

<?php get_template_part('templates/header'); ?>

<div class="container">
    <div class="content">
        <h1>This is page.php</h1>

        <?php get_template_part('templates/loop'); ?>
    </div>
</div>

<?php get_template_part('templates/footer'); ?>
