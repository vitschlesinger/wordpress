<?php get_template_part('templates/header'); ?>

<div class="container">
    <div class="content">
        <h1>This is search.php</h1>

        <p><?php echo sprintf(__('%s Search Results for ', 'wordpress'), $wp_query->found_posts);
            echo get_search_query(); ?></p>

        <?php get_template_part('templates/loop', 'single'); ?>

    </div>
</div>

<?php get_template_part('templates/footer'); ?>
