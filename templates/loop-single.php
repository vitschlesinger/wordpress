<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <h2><?php the_title(); ?></h2>

    <p>Author: <?php the_author(); ?></p>

    <p>Posted: <?php the_date('j. F, Y') ?></p>

    <p>Category: <?php the_category(', '); ?></p>

    <?php the_content(); ?>

<?php endwhile;

else: ?>

    <p><?php __('Sorry, this page does not exist.'); ?></p>

<?php endif; ?>
