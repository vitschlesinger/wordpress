<h3><?php comments_number('No Responses', 'One Response', '% Responses'); ?> to “<?php the_title(); ?>”</h3>

<?php $comments_arg = array(
    'comment_notes_after' => ''
); ?>
<?php comment_form($comments_arg); ?>

