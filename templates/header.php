<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php wp_title(''); ?></title>

    <meta name="description" content="<?php bloginfo('description'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php wp_head(); ?>
</head>
<body>

<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

<div class="snap-drawers">
    <div class="snap-drawer snap-drawer-right">
        <div>
            <h4><?php echo __('Navigace'); ?></h4>
            <?php wp_nav_menu(array(
                'container' => false,
                'container_class' => '',
                'menu' => __('The Main Menu'),
                'menu_id' => 'menu-mobile',
                'menu_class' => '',
                'theme_location' => 'primary-menu',
                'before' => '',
                'after' => '',
                'link_before' => '',
                'link_after' => '',
                'depth' => 0,
                'fallback_cb' => ''
            )); ?>
        </div>
    </div>
</div>

<div id="wrapper" class="snap-content">
    <div class="navigation">
        <div class="container">
            <a class="brand" href="<?php echo home_url(); ?>">
                <img src="<?php echo get_template_directory_uri(); ?>/static/img/logo-white.svg" alt="Logo"
                     class="image-svg">
                <img src="<?php echo get_template_directory_uri(); ?>/static/img/logo-white.png" alt="Logo"
                     class="image-png">
            </a>

            <a href="#" id="open-right">
                <span class="menu-toggle-title">
                    Menu
                </span>
                <ul class="menu-toggle">
                    <li class="first"></li>
                    <li class="second"></li>
                    <li class="third"></li>
                </ul>
            </a>

            <?php wp_nav_menu(array(
                'container' => false,
                'container_class' => '',
                'menu' => __('The Main Menu'),
                'menu_id' => 'menu',
                'menu_class' => '',
                'theme_location' => 'primary-menu',
                'before' => '',
                'after' => '',
                'link_before' => '',
                'link_after' => '',
                'depth' => 0,
                'fallback_cb' => ''
            )); ?>
        </div>


    </div>



