<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <a href="<?php the_permalink() ?>"><h2><?php the_title(); ?></h2></a>

    <p>Author: <?php the_author_posts_link(); ?></p>

    <p>Posted: <?php the_time('j. F, Y') ?></p>

    <p>Category: <?php the_category(', '); ?></p>

    <p><?php the_tags('Tags: ', ', '); ?></p>

    <p><?php comments_number('no response', '1 response', '% responses'); ?> </p>

    <?php the_content('Continue ...'); ?>

<?php endwhile; ?>

    <?php get_template_part('templates/pagination'); ?>

<?php else: ?>

    <p>Sorry, no post to list.</p>

<?php endif; ?>
