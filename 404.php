<?php get_template_part('templates/header'); ?>

<div class="container">
    <div class="content">
        <h1>This is 404.php</h1>

        <p><?php echo __('This page does not exist. You can search for another page.'); ?></p>
    </div>
</div>

<?php get_search_form(); ?>

<?php get_template_part('templates/footer'); ?>
